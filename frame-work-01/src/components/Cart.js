import React, { Component } from 'react';
// import Cart from './Cart';

class Cart extends Component {
  constructor(props) {
    super(props) // 将父组件引用处声明的属性加入子类的this.props。例子里的title：<Cart title='我的购物车'></Cart>
    console.log(props.data)
    this.state = { // 声明组件的状态，类似与vue组件的data
      cartList: props.data || [] // 购物车列表
    }
  }

  render() { // 渲染jsx的函数
    return (
      <div>
        <ul style={{ textAlign: 'right', paddingRight: '100px' }}>
          {this.state.cartList.map(good => (
            <li key={good.id}>{good.name}</li>
          ))}
        </ul>
      </div>
    );
  }
}
export default Cart;