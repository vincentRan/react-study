import React, { Component } from 'react';

class Clock extends Component {
  state = {
    date: new Date(),
    time: new Date()
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({
        date: new Date()
      });
      this.setState({
        time: new Date()
      });
    }, 1000)
  }
  componentWillUnmount() {
    clearInterval(this.timer)
    this.timer = null;
  }
  render() {
    return (
      <div>
        <span>{this.state.date.toLocaleTimeString()}</span>
        <p>{this.state.time.toLocaleTimeString()}</p>
      </div>
    );
  }
}

export default Clock;