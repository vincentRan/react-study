export default function debounce(fn, interval) {
	const self = fn;
	let timer = null;
	let firstTime = true;

	return function(...args) {
		const that = this;
		if (firstTime) {
			self.apply(that, args)
			return (firstTime = false)
		}

		if (timer) {
			return false
		}

		timer = setTimeout(function() {
			clearTimeout(timer)
			timer = null;
			self.apply(that, args)
		}, interval || 500)
	}
}
// (fn, interval) {
//   const self = fn;

//   let timer = null;
//   let flag = true;

//   return function (...args) {
//     console.log(args);
//     const that = this;
//     if (flag) {
//       self.apply(that, args);
//       return (flag = false);
//     }
//     if (timer) {
//       clearTimeout(timer);
//       timer = null;
//       return false
//     }
//     timer = setTimeout(function () {
//       clearTimeout(timer);
//       timer = null;

//       self.apply(that, args);
//     }, interval || 500);
//   }
// }