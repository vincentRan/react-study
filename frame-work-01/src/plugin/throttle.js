export default function throttle(fn, interval) {
  let timer = null;
  let flag = true;
  return (...args) => {
    if (flag) {
      fn.apply(this, args);
      return flag = false;
    }
    if (timer) return false;
    timer = setTimeout(() => {
      clearTimeout(timer);
      timer = null;

      fn.apply(this, args)
    }, interval || 500);
  }
}