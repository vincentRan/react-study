import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { Welecome1, Welecome2 } from './components/CompType.js';
import Clock from './components/Clock';
import CartSample from './components/CartSample';
import Debounce from './plugin/debounce.js';
import Throttle from './plugin/throttle.js';

function App() {
  const data = {
    count: 0,
    title: 'react',
  }

  return (
    <div className="App">
      <h1>hello {data.title}</h1>
      <button onClick={
        Throttle(() => {
          data.count++;
          data.title = `Throttle${data.count}`;
        }, 200)
      }>节流</button>
      <button onClick={Debounce(() => {

        alert('不管你点得多快，我在 500ms 内只触发一次')

      }, 500)}>防抖</button>
      <button onClick={Debounce(() => {

        alert('不管你点得多快，我在 500ms 内只触发一次')

      }, 500)}>完美体验</button>
      <Welecome1></Welecome1>
      <Welecome2></Welecome2>
      <Clock></Clock>
      <hr />
      <CartSample title='我的购物车'></CartSample>
    </div>
  );
}

export default App;
